
const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/myProject')
const Schema = mongoose.Schema
const UserSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    createdAt:{
        type: Date,
        default: Date.now
    }
},{
    collection:'Users'
})

UserAccount = mongoose.model('users', UserSchema)

module.exports = UserAccount




