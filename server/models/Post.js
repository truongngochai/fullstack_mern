const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/myProject')
const Schema = mongoose.Schema
const status = ['TO LEARN', 'LEARNING', 'LEARNED']
const PostSchema = new Schema({
    title: {
        type: String,
        require: true
    },
    description: {
        type: String,
    },
    url: {
        type: String,
    },
    status: {
        type: String,
        enum: status
    },
    user: {
        type: Schema.Types.ObjectId,
        ref:'Users'
    }

},{
    collection:'Posts'
})

module.exports = mongoose.model('posts', PostSchema)