require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')

const authRouter = require('./routers/auth')
const postRouter = require('./routers/post')

const connectDB = async() => {
    try {
        await mongoose.connect(`mongodb+srv://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@mern-learnit.oggdp.mongodb.net/mern-learnit?retryWrites=true&w=majority`, {
        // useCreateIndex: true,
        //non support mongoose 6
        useNewUrlParser: true,
        useUnifiedTopology: true,
        // useFindAndModify: false
    })
    console.log('MongoDB connected');
        // myFirstDatabase -> mern-learnit
    } catch (error) {
        console.log(error.message)
        process.exit(1)
    }
}
connectDB()

const app = express()

// app.get('/',(req,res) => {res.send('Hello World, Hai')})
app.use(express.json())
app.use('/api/auth', authRouter)
app.use('/api/posts', postRouter)

const PORT = 5000

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))